@extends('index')
@section('breadcrumbs')
    <ul class="breadcrumb">
        <li class="breadcrumb-item">
            <span>User Profil</span>
        </li>
    </ul>
@endsection
@push('head')
@endpush
@section('content')
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="element-box">
                        <form id="formValidate" action="{{route('user-profile.update')}}" method="post">
                            @csrf
                            <div class="element-info">
                                <div class="element-info-with-icon">
                                    <div class="element-info-icon">
                                        <div class="os-icon os-icon-wallet-loaded"></div>
                                    </div>
                                    <div class="element-info-text">
                                        <h5 class="element-inner-header">
                                            Profile Settings
                                        </h5>
                                        <div class="element-inner-desc">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if (session()->has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true"> ×</span></button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for=""> Name</label><input name="nama" value="{{$user->nama}}"
                                                                  class="form-control" data-error="Your name is invalid"
                                                                  placeholder="Enter Name" required="required"
                                                                  type="text"/>
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for=""> Email address</label><input value="{{$user->email}}" name="email"
                                                                           class="form-control"
                                                                           data-error="Your email address is invalid"
                                                                           placeholder="Enter email" required="required"
                                                                           type="email"/>
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for=""> Password</label><input value="" name="password" class="form-control"
                                                                              data-minlength="6" placeholder="Isi Password Untuk Mengganti Password"
                                                                              type="password"/>
                                        <div class="help-block form-text text-muted form-control-feedback">
                                            Minimum of 6 characters
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-buttons-w">
                                <button class="btn btn-primary" type="submit"> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
