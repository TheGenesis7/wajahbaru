@extends('index')
@section('breadcrumbs')
    <ul class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('profile')}}">Profile</a>
        </li>
        <li class="breadcrumb-item">
            <a href="">Update Profile</a>
        </li>
    </ul>
@endsection
@section('content')
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-box">
                                <div class="element-info">
                                    <div class="element-info-with-icon">
                                        <div class="element-info-icon">
                                            <div class="os-icon os-icon-user"></div>
                                        </div>
                                        <div class="element-info-text">
                                            <h5 class="element-inner-header">
                                                Update Profile
                                            </h5>
                                            <div class="element-inner-desc">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-desc">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if (session()->has('success'))
                                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true"> ×</span></button>
                                                    <strong>{{session('success')}}</strong>
                                                </div>
                                            @endif
                                            @if (session()->has('error'))
                                                <div class="alert alert-error alert-dismissible fade show" role="alert">
                                                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true"> ×</span></button>
                                                    <strong>{{session('error')}}</strong>
                                                </div>
                                            @endif
                                            <form id="formValidate" action="{{route('profile.update',['id' => $profile->id])}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                @method('put')
                                                <fieldset class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <a href="{{route('timeline',['id_profile' => $profile->id])}}" class="btn btn-success pull-right"> Tambahkan Timeline</a>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Singkat Satu</label>
                                                                <input type="text" class="form-control" data-error="Title Depan Harus di isi" name="title_depan" value="{{$profile->title_depan}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Singkat Dua</label>
                                                                <input type="text" class="form-control" data-error="Title Belakang Harus di isi" name="title_belakang" value="{{$profile->title_belakang}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Singkat Tiga</label>
                                                                <input type="text" class="form-control" data-error="Nama Belakang Harus di isi" name="line_link" type="text" value="{{$profile->line_link}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Pengalaman Tiga</label>
                                                                <input type="text" class="form-control" data-error="Nama Depan Harus di isi" name="nama_depan" value="{{$profile->nama_depan}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Pengalaman Dua</label>
                                                                <input type="text" class="form-control" data-error="Nama Belakang Harus di isi" name="nama_belakang" value="{{$profile->nama_belakang}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Pengalaman Satu</label>
                                                                <input type="text" class="form-control" data-error="Nama Belakang Harus di isi" value="{{$profile->fb_link}}" name="fb_link" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                            <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Karir Satu</label>
                                                                <input type="text" class="form-control" data-error="Nama Belakang Harus di isi" value="{{$profile->instagram_link}}" name="instagram_link" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Karir Dua</label>
                                                                <input type="text" class="form-control" data-error="Nama Belakang Harus di isi" name="twitter_link"  type="text" value="{{$profile->twitter_link}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Karir Tiga</label>
                                                                <input type="text" class="form-control" data-error="Nama Belakang Harus di isi" name="youtube_link"  type="text" value="{{$profile->youtube_link}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label for=""> Gambar Profile</label>
                                                                <input accept="image/*" class="form-control img-input" name="gambar_profile" value="" data-error="Gambar Profile Harus di isi !" placeholder="Gambar Profile" type="file" />
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <img src="{{$profile->gambar_profile_path}}" alt="" class="img-view img-fluid ">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label for=""> Gambar Profile</label>
                                                                <input accept="image/*" class="form-control img-input" name="gambar_profile_2" value="" data-error="Gambar Profile Harus di isi !" placeholder="Gambar Profile 2" type="file" />
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <img src="{{$profile->gambar_profile_2_path}}" alt="" class="img-view img-fluid ">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div class="form-buttons-w">
                                                    <button class="btn btn-primary" type="submit"> Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
