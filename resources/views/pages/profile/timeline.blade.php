@extends('index')
@section('breadcrumbs')
    <ul class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('profile')}}">Profile</a>
        </li>
        <li class="breadcrumb-item">
            <a href="">Profile Timeline</a>
        </li>
    </ul>
@endsection
@section('content')
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-box">
                                <div class="element-info">
                                    <div class="element-info-with-icon">
                                        <div class="element-info-icon">
                                            <div class="os-icon os-icon-user"></div>
                                        </div>
                                        <div class="element-info-text">
                                            <h5 class="element-inner-header">
                                                Timeline Profile
                                            </h5>
                                            <div class="element-inner-desc">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-header">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>
                                                Data Timeline
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-desc">
                                    @if (session()->has('success'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true"> ×</span></button>
                                            <strong>{{session('success')}}</strong>
                                        </div>
                                    @endif
                                    <div class="table-responsive">
                                        <table class="dt table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Tahun</th>
                                                <th>Deskripsi</th>
                                                <th>Gambar</th>
                                                <th>..</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>Tahun</th>
                                                <th>Deskripsi</th>
                                                <th>Gambar</th>
                                                <th></th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            @foreach($timeline AS $row)
                                                <tr>
                                                    <td>{{$row->tahun}}</td>
                                                    <td>{{$row->deskripsi_timeline}}</td>
                                                    <td><img src="{{$row->gambar_timeline_path}}" class="img-fluid" alt="" style="width: 200px"></td>
                                                    <td>
                                                        <a href="{{route('timeline.detail',['id_profile' => $id_profile,'id' => $row->id])}}" class="btn btn-primary btn-detail"><i class="fa fa-edit"></i></a>
                                                        <form action="{{route('timeline.delete',['id_profile' => $id_profile, 'id' => $row->id])}}" method="post" class="no-form">
                                                            @csrf
                                                            @method('delete')
                                                            <a href="" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-header">
                                    <div class="form-group">
                                        <h5>Tambah / Edit Timeline</h5>
                                    </div>
                                    <form action="{{route('timeline.post',['id_profile' => $id_profile])}}" id="form-timeline" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="id">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="">Tahun</label>
                                                    <input type="number" class="form-control" data-error="Tahun Harus di isi" name="tahun" value="{{old('tahun')}}" required>
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="">Deskripsi Timeline</label>
                                                    <textarea class="form-control" data-error="Deskripsi Timeline Harus di isi" name="deskripsi_timeline" required>{{old('deskripsi_timeline')}}</textarea>
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label for=""> Gambar Timeline</label>
                                                    <input accept="image/*" class="form-control img-input" name="gambar_timeline" value="" data-error="Gambar Timeline Harus di isi !" placeholder="Gambar Profile" required type="file" />
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <img src="" alt="" class="img-view img-fluid ">
                                            </div>
                                        </div>
                                        <div class="form-buttons-w">
                                            <button class="btn btn-primary" type="submit"> Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('.btn-detail').on('click',function (e) {
            e.preventDefault();
            $.get(this.href)
                .then(function(res){
                    $('[name=id]').val(res.id);
                    $('[name=tahun]').val(res.tahun);
                    $('[name=deskripsi_timeline]').val(res.deskripsi_timeline);
                    $('.img-view').attr('src',res.gambar_timeline_path);
                    $('[name=gambar_timeline]').attr('required',false);
                })
        })
        $('.btn-delete').on('click',function (e) {
            e.preventDefault();
            if (confirm('Yakin Ingin Menghapus Berita ?')){
                $(this).parent().submit();
                return true;
            }
            return false;
        })
    </script>
@endpush
