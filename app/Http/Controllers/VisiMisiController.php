<?php

namespace App\Http\Controllers;

use App\Model\VisiMisi;
use Illuminate\Http\Request;

class VisiMisiController extends Controller
{
    public function index(){
        $visi_misi = VisiMisi::orderBy('jenis','asc')->get();
        return view('pages.visi-misi.index',compact('visi_misi'));
    }

    public function post(Request $request){
        if ($request->has('id')){
            $visi_misi = VisiMisi::find($request->id);
        }
        $this->validate($request,[
            'jenis' => 'required',
            'deskripsi' => 'required',
        ]);
        $data = $request->all();
        if ($visi_misi){
            $visi_misi->update($data);
            $message = 'Berhasil Mengupdate Visi Misi';
        } else {
            VisiMisi::create($data);
            $message = 'Berhasil Menambahkan Visi Misi';
        }
        return redirect()->back()->with('success',$message);
    }

    public function detail(Request $request,$id){
        $visi_misi = VisiMisi::find($id);
        return response()->json($visi_misi);
    }

    public function delete($id){
        $visi_misi = VisiMisi::find($id);
        $visi_misi->delete();
        return redirect()->back();
    }
}
