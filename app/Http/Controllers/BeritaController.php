<?php

namespace App\Http\Controllers;

use App\Model\Berita;
use Illuminate\Http\Request;

class BeritaController extends Controller
{
    public function index(){
        $berita = Berita::orderBy('created_at')->get();
        return view('pages.berita.index',compact('berita'));
    }

    public function add(){
        return view('pages.berita.add');
    }

    public function detail($id){
        $berita = Berita::find($id);
        return view('pages.berita.detail',compact('berita'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'gambar' => 'required|image',
            'excerpt' => 'required',
            'konten' => 'required',
            'judul' => 'required'
        ]);
        $data = $request->all();
        $data['gambar'] = $request->file('gambar')->store('public/berita');
        if (Berita::create($data)) {
            return redirect()->route('berita')->with('success', 'Berhasil Menambah Berita');
        }
        return redirect()->back()->with('error','Gagal Menambah Data');
    }

    public function update(Request $request,$id){
        $berita = Berita::find($id);
        $this->validate($request,[
            'gambar' => 'image',
            'excerpt' => 'required',
            'konten' => 'required',
            'judul' => 'required'
        ]);
        $data = $request->all();
        if ($request->hasFile('gambar')){
            if (file_exists(storage_path('app/'.$berita->gambar)))
                unlink(storage_path('app/'.$berita->gambar));
            $data['gambar'] = $request->file('gambar')->store('public/berita');
        }
        if ($berita->update($data)){
            return redirect()->back()->with('success','Berhasil Mengupdate Berita');
        }
        return redirect()->back()->with('error','Gagal Mengupdate Data');
    }

    public function delete($id){
        $berita = Berita::find($id);
        if (file_exists(storage_path('app/'.$berita->gambar)))
            unlink(storage_path('app/'.$berita->gambar));
        $berita->delete();
        return redirect()->route('berita');
    }

}
