<?php
/**
 * Created by PhpStorm.
 * User: alifdoco
 * Date: 2018-11-27
 * Time: 17:20
 */

namespace App\Helper;


class Response
{
    const STATUS_OK = 200;
    const STATUS_VALIDATION_ERROR = 422;
    const STATUS_INTERNAL_SERVER_ERROR = 500;
    const STATUS_BAD_REQUEST = 400;
    const STATUS_NOT_FOUND = 404;
    const STATUS_DELETED = 204;

    const MESSAGE_200 = 'ok';
    const MESSAGE_422 = 'missing required parameter';
    const MESSAGE_500 = 'internal server error';
    const MESSAGE_400 = 'bad request';
    const MESSAGE_404 = 'not found';
    const MESSAGE_204 = 'succesfully delete';
    const MESSAGE_UPDATE = 'succesfully update';
    const MESSAGE_INSERT = 'succesfully insert';

    static public function json($data,$message = self::MESSAGE_200, $status = self::STATUS_OK)
    {
        if ($data == null){
            return self::notFound();
        }
        $dgn = [
            'status' => $status,
            'message' => $message
        ];
        $response = [
            'diagnostic' => $dgn,
            'response' => $data
        ];
        return response()->json($response);
    }

    static public function validationError($required_parameter){
        $dgn = [
            'status' => self::STATUS_VALIDATION_ERROR,
            'message' => self::MESSAGE_422,
            'required_parameter' => $required_parameter
        ];
        return response()->json([
            'diagnostic' => $dgn
        ]);
    }

    static public function error($message = self::MESSAGE_500){
        $dgn = [
            'status' => self::STATUS_INTERNAL_SERVER_ERROR,
            'message' => $message
        ];
        return response()->json([
            'diagnostic' => $dgn
        ]);
    }

    static public function notFound($message = self::MESSAGE_404){
        $dgn = [
            'status' => self::STATUS_NOT_FOUND,
            'message' => $message
        ];
        return response()->json([
            'diagnostic' => $dgn
        ]);
    }

    static public function successDelete($deleted, $message = self::MESSAGE_204){
        if ($deleted) {
            $dgn = [
                'status' => self::STATUS_DELETED,
                'message' => $message
            ];
        } else {
            return self::notFound();
        }
        return response()->json([
            'diagnostic' => $dgn
        ]);
    }

    static public function successUpdate($data, $message = self::MESSAGE_UPDATE){
        $dgn = [
            'status' => self::STATUS_OK,
            'message' => $message
        ];
        $response = [
            'diagnostic' => $dgn,
            'response' => $data
        ];
        return response()->json($response);
    }
}
