<?php

namespace App\Http\Controllers;

use App\Model\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::orderBy('created_at')->get();
        return view('pages.profile.index',compact('profile'));
    }

    public function add(){
        return view('pages.profile.add');
    }

    public function detail($id){
        $profile = Profile::find($id);
        return view('pages.profile.detail',compact('profile'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'gambar_profile' => 'required|image',
            'gambar_profile_2' => 'required|image',
            'title_depan' => 'required',
            'title_belakang' => 'required',
            'nama_depan' => 'required',
            'nama_belakang' => 'required',
            'fb_link' => 'required',
            'twitter_link' => 'required',
            'instagram_link' => 'required',
            'youtube_link' => 'required',
            'line_link' => 'required',
        ]);
        $data = $request->all();
        $data['gambar_profile'] = $request->file('gambar_profile')->store('public/profile');
        $data['gambar_profile_2'] = $request->file('gambar_profile_2')->store('public/profile');
        if (Profile::create($data)) {
            return redirect()->route('profile')->with('success', 'Berhasil Menambah Profile');
        }
        return redirect()->back()->with('error','Gagal Menambah Data');
    }

    public function update(Request $request,$id){
        $profile = Profile::find($id);
        $this->validate($request,[
            'gambar_profile' => 'image',
            'gambar_profile_2' => 'image',
            'title_depan' => 'required',
            'title_belakang' => 'required',
            'nama_depan' => 'required',
            'nama_belakang' => 'required',
            'fb_link' => 'required',
            'twitter_link' => 'required',
            'instagram_link' => 'required',
            'youtube_link' => 'required',
            'line_link' => 'required',
        ]);
        $data = $request->all();
        if ($request->hasFile('gambar_profile')){
            if (file_exists(storage_path('app/'.$profile->gambar_profile)))
                unlink(storage_path('app/'.$profile->gambar_profile));
            $data['gambar_profile'] = $request->file('gambar_profile')->store('public/profile');
        }
        if ($request->hasFile('gambar_profile_2')){
            if (file_exists(storage_path('app/'.$profile->gambar_profile_2)))
                unlink(storage_path('app/'.$profile->gambar_profile_2));
            $data['gambar_profile_2'] = $request->file('gambar_profile_2')->store('public/profile');
        }
        if ($profile->update($data)){
            return redirect()->back()->with('success','Berhasil Mengupdate Profile');
        }
        return redirect()->back()->with('error','Gagal Mengupdate Data');
    }

    public function delete($id){
        $profile = Profile::find($id);
        $gambar = storage_path('app/'.$profile->gambar);
        if (file_exists($gambar))
            unlink($gambar);
        $profile->delete();
        return redirect()->route('profile');
    }

}
