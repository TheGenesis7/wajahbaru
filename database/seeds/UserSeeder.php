<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'nama' => 'User',
                'email' => 'user@email.com',
                'no_telp' => '0812414141',
                'password' => bcrypt('password'),
            ]
        ];

        DB::table('users')->insert($users);
    }
}
