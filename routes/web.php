<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'auth'], function () {
    Route::get('home', 'WebController@index')->name('web');
    Route::post('web-setting', 'WebController@store')->name('web.post');
    Route::get('user-profile', 'Controller@profile')->name('user-profile');
    Route::post('user-profile', 'Controller@updateProfile')->name('user-profile.update');

    Route::group(['prefix' => 'berita'], function () {
        Route::get('', 'BeritaController@index')->name('berita');
        Route::get('add', 'BeritaController@add')->name('berita.add');
        Route::get('{id}', 'BeritaController@detail')->name('berita.detail');
        Route::post('', 'BeritaController@store')->name('berita.post');
        Route::put('{id}', 'BeritaController@update')->name('berita.update');
        Route::delete('{id}', 'BeritaController@delete')->name('berita.delete');
    });
    Route::group(['prefix' => 'testimoni'], function () {
        Route::get('', 'TestimoniController@index')->name('testimoni');
        Route::get('add', 'TestimoniController@add')->name('testimoni.add');
        Route::get('{id}', 'TestimoniController@detail')->name('testimoni.detail');
        Route::post('', 'TestimoniController@store')->name('testimoni.post');
        Route::put('{id}', 'TestimoniController@update')->name('testimoni.update');
        Route::delete('{id}', 'TestimoniController@delete')->name('testimoni.delete');
    });
    Route::group(['prefix' => 'relawan'], function () {
        Route::get('', 'RelawanController@index')->name('relawan');
        Route::get('add', 'RelawanController@add')->name('relawan.add');
        Route::get('{id}', 'RelawanController@detail')->name('relawan.detail');
        Route::post('', 'RelawanController@store')->name('relawan.post');
        Route::put('{id}', 'RelawanController@update')->name('relawan.update');
        Route::delete('{id}', 'RelawanController@delete')->name('relawan.delete');
    });
    Route::group(['prefix' => 'visi_misi'], function () {
        Route::get('{id}', 'VisiMisiController@detail')->name('visi_misi.detail');
        Route::delete('{id}', 'VisiMisiController@delete')->name('visi_misi.delete');
        Route::get('', 'VisiMisiController@index')->name('visi_misi');
        Route::post('', 'VisiMisiController@post')->name('visi_misi.post');
    });
    Route::group(['prefix' => 'profile'], function () {
        Route::group(['prefix' => 'timeline'], function () {
            Route::get('{id_profile}/{id}', 'TimelineController@detail')->name('timeline.detail');
            Route::delete('{id_profile}/{id}', 'TimelineController@delete')->name('timeline.delete');
            Route::get('{id_profile}', 'TimelineController@index')->name('timeline');
            Route::post('{id_profile}', 'TimelineController@post')->name('timeline.post');
        });
        Route::get('', 'ProfileController@index')->name('profile');
        Route::get('add', 'ProfileController@add')->name('profile.add');
        Route::get('{id}', 'ProfileController@detail')->name('profile.detail');
        Route::post('', 'ProfileController@store')->name('profile.post');
        Route::put('{id}', 'ProfileController@update')->name('profile.update');
        Route::delete('{id}', 'ProfileController@delete')->name('profile.delete');
    });
});
Auth::routes();
Route::get('/', function () {
    return view('home');
});

Route::get('/profil', function() {
    return view('profil');
});

Route::get('/news/{id}', function($id) {
    return view('berita')->withData($id);
});

Route::get('/visi', function() {
    return view('visi');
});