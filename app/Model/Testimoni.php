<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Testimoni extends Model
{
    protected $table = 'testimoni';
    protected $fillable = [
        'gambar',
        'nama',
        'testimoni'
    ];
    public $appends = [
        'gambar_path'
    ];
    public function getGambarPathAttribute(){
        return asset('storage/'.str_replace('public/','',$this->gambar));
    }
}
