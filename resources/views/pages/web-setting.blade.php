@extends('index')
@section('breadcrumbs')
    <ul class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{url('/')}}">Web Setting</a>
        </li>
    </ul>
@endsection
@section('content')
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-box">
                                <div class="element-info">
                                    <div class="element-info-with-icon">
                                        <div class="element-info-icon">
                                            <div class="os-icon os-icon-monitor"></div>
                                        </div>
                                        <div class="element-info-text">
                                            <h5 class="element-inner-header">
                                                Web Setting
                                            </h5>
                                            <div class="element-inner-desc">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-desc">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form id="formValidate" action="{{route('web.post')}}" method="post" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                @if (session()->has('success'))
                                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true"> ×</span></button>
                                                    <strong>{{session('success')}}</strong>
                                                </div>
                                                @endif
                                                <fieldset class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label for=""> Logo</label>
                                                                <input accept="image/*" class="form-control img-input" name="logo" value="" data-error="Logo Harus di isi !" placeholder="Logo Web" {{$web ? $web->logo ? '' : 'required' : ''}} type="file" />
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <img src="{{$web ? $web->logo_path : old('logo')}}" alt="" class="img-view img-fluid ">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group form-buttons-w">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Tagline</label>
                                                                <textarea class="form-control ckeditor" data-error="Tagline Harus di isi !" name="tagline" id="" required>{{$web ? $web->tagline : old('tagline')}}</textarea>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group form-buttons-w">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="fa fa-youtube-play fa-2x"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input class="form-control" name="video" data-error="Youtube Vide Link Harus di isi !" placeholder="Youtube Video Link" type="text" value="{{$web ? $web->video : old('video')}}" required>
                                                                </div>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Deskripsi Video</label>
                                                                <textarea class="form-control ckeditor" data-error="Deskripsi Video Harus di isi !" name="deskripsi_video" id="" required>{{$web ? $web->deskripsi_video : old('deskripsi_video')}}</textarea>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group form-buttons-w">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="fa fa-facebook-square fa-2x"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input class="form-control" data-error="Facebook Link Harus di isi !" name="facebook" placeholder="Facebook Link" type="text" value="{{$web ? $web->facebook : old('facebook')}}" required>
                                                                </div>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="fa fa-instagram fa-2x"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input class="form-control" data-error="Instagram Link Harus di isi !" name="instagram" placeholder="Instagram Link" value="{{$web ? $web->instagram : old('instagram')}}" type="text" required>
                                                                </div>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="fa fa-twitter-square fa-2x"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input class="form-control" data-error="Twitter Link Harus di isi !" name="twitter" placeholder="Twitter Link" type="text" value="{{$web ? $web->twitter : old('twitter')}}" required>
                                                                </div>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="fa fa-youtube-square fa-2x"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input class="form-control" data-error="Youtube Channel Link Harus di isi !" name="youtube" placeholder="Youtube Channel Link" type="text" value="{{$web ? $web->youtube : old('youtube')}}" required>
                                                                </div>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="fa fa-envelope fa-2x"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input class="form-control" data-error="Email Harus di isi !" name="email" placeholder="Email" type="email" value="{{$web ? $web->email : old('email')}}" required>
                                                                </div>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group form-buttons-w">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Alamat</label>
                                                                <textarea class="form-control ckeditor" data-error="Alamat Harus di isi !" name="alamat" id="" required>{{$web ? $web->alamat : old('alamat')}}</textarea>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div class="form-buttons-w">
                                                    <button class="btn btn-primary" type="submit"> Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
