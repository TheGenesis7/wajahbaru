@extends('index')
@section('breadcrumbs')
    <ul class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('relawan')}}">Relawan</a>
        </li>
        <li class="breadcrumb-item">
            <a href="">Update Relawan</a>
        </li>
    </ul>
@endsection
@section('content')
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-box">
                                <div class="element-info">
                                    <div class="element-info-with-icon">
                                        <div class="element-info-icon">
                                            <div class="os-icon os-icon-users"></div>
                                        </div>
                                        <div class="element-info-text">
                                            <h5 class="element-inner-header">
                                                Update Relawan
                                            </h5>
                                            <div class="element-inner-desc">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-desc">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form id="formValidate" action="{{route('relawan.update',['id' => $relawan->id])}}" method="post">
                                                {{csrf_field()}}
                                                @method('put')
                                                @if (session()->has('success'))
                                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true"> ×</span></button>
                                                    <strong>{{session('success')}}</strong>
                                                </div>
                                                @endif
                                                @if (session()->has('error'))
                                                <div class="alert alert-error alert-dismissible fade show" role="alert">
                                                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true"> ×</span></button>
                                                    <strong>{{session('error')}}</strong>
                                                </div>
                                                @endif
                                                <fieldset class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Nama</label>
                                                                <input type="text" class="form-control" data-error="Nama Harus di isi" name="nama" value="{{$relawan ? $relawan->nama : old('nama')}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">NIK / KTP</label>
                                                                <input type="text" class="form-control" data-error="NIK / KTP Harus di isi" name="nik_ktp" value="{{$relawan ? $relawan->nik_ktp : old('nik_ktp')}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Email</label>
                                                                <input type="email" class="form-control" data-error="Email Harus di isi" name="email" value="{{$relawan ? $relawan->email : old('email')}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">No Hp</label>
                                                                <input type="text" class="form-control" data-error="No Hp Harus di isi" name="no_hp" value="{{$relawan ? $relawan->no_hp : old('no_hp')}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Alamat</label>
                                                                <textarea class="form-control" data-error="Alamat Harus di isi !" name="alamat" id="" required>{{$relawan ? $relawan->alamat : old('alamat')}}</textarea>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div class="form-buttons-w">
                                                    <button class="btn btn-primary" type="submit"> Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
