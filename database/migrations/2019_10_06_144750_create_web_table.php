<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('logo');
            $table->string('tagline');
            $table->string('video');
            $table->string('deskripsi_video');
            $table->string('facebook');
            $table->string('instagram');
            $table->string('twitter');
            $table->string('email');
            $table->string('youtube');
            $table->string('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('web');
    }
}
