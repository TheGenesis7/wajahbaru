<?php

namespace App\Http\Controllers;

use App\Model\Relawan;
use Illuminate\Http\Request;

class RelawanController extends Controller
{
    public function index(){
        $relawan = Relawan::orderBy('created_at')->get();
        return view('pages.relawan.index',compact('relawan'));
    }

    public function add(){
        return view('pages.relawan.add');
    }

    public function detail($id){
        $relawan = Relawan::find($id);
        return view('pages.relawan.detail',compact('relawan'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'nama' => 'required',
            'nik_ktp' => 'required',
            'email' => 'required|email',
            'no_hp' => 'required',
            'alamat' => 'required',
        ]);
        $data = $request->all();
        if (Relawan::create($data)) {
            return redirect()->route('relawan')->with('success', 'Berhasil Menambah Relawan');
        }
        return redirect()->back()->with('error','Gagal Menambah Data');
    }

    public function update(Request $request,$id){
        $relawan = Relawan::find($id);
        $this->validate($request,[
            'nama' => 'required',
            'nik_ktp' => 'required',
            'email' => 'required|email',
            'no_hp' => 'required',
            'alamat' => 'required',
        ]);
        $data = $request->all();
        if ($relawan->update($data)){
            return redirect()->back()->with('success','Berhasil Mengupdate Relawan');
        }
        return redirect()->back()->with('error','Gagal Mengupdate Data');
    }

    public function delete($id){
        $relawan = Relawan::find($id);
        $relawan->delete();
        return redirect()->route('relawan');
    }

}
