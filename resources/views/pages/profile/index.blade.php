@extends('index')
@section('breadcrumbs')
    <ul class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="">Profile</a>
        </li>
    </ul>
@endsection
@section('content')
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-box">
                                <div class="element-info">
                                    <div class="element-info-with-icon">
                                        <div class="element-info-icon">
                                            <div class="os-icon os-icon-user"></div>
                                        </div>
                                        <div class="element-info-text">
                                            <h5 class="element-inner-header">
                                                Profile
                                            </h5>
                                            <div class="element-inner-desc">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-header">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="{{route('profile.add')}}" class="btn btn-primary pull-right">Tambah Profile</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-desc">
                                    @if (session()->has('success'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true"> ×</span></button>
                                            <strong>{{session('success')}}</strong>
                                        </div>
                                    @endif
                                    <div class="table-responsive">
                                        <table class="dt table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>..</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>Nama</th>
                                                <th></th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            @foreach($profile AS $row)
                                                <tr>
                                                    <td>{{$row->title_depan}} {{$row->nama_depan}} {{$row->nama_belakang}} {{$row->title_belakang}}</td>
                                                    <td>
                                                        <a href="{{route('profile.detail',['id' => $row->id])}}" title="detail" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                                        <a href="{{route('timeline',['id_profile' => $row->id])}}" title="timeline" class="btn btn-success"><i class="fa fa-calendar-times-o"></i></a>
                                                        <form action="{{route('profile.delete',['id' => $row->id])}}" method="post" class="no-form">
                                                            @csrf
                                                            @method('delete')
                                                            <a href="" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('.btn-delete').on('click',function (e) {
            e.preventDefault();
            if (confirm('Yakin Ingin Menghapus Berita ?')){
                $(this).parent().submit();
                return true;
            }
            return false;
        })
    </script>
@endpush
