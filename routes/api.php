<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('web-setting','ApiController@getWebSetting');
Route::get('berita','ApiController@getBerita');
Route::get('berita/{id}','ApiController@getDetailBerita');
Route::get('testimoni','ApiController@getTestimoni');
Route::get('profile','ApiController@getProfile');
Route::get('profile/{id}','ApiController@getDetailProfile');
Route::get('visi-misi','ApiController@getVisiMisi');
Route::post('relawan','ApiController@postRelawan');
