require('./bootstrap');

window.Vue = require('vue');
// import VueRouter from "vue-router";
// Vue.use(VueRouter);
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
Vue.use(Buefy)

import Home from './components/Home';
import Profil from './components/Profile';


// let routes = [{
//         path: "/",
//         component: require('./components/App.vue'),
//     },
//     {
//         path: "/profil",
//         component: Profil,
//     },
// ];

import moment from "moment";

// const router = new VueRouter({
//     mode: "history",
//     routes // short for `routes: routes`
// });

Vue.filter("myDate", function (created) {
    return moment(created).format("MMMM Do YYYY");
});


const app = new Vue({
    el: '#app',
    render: h => h(Home)
});

import Berita from './components/IsiBerita.vue';

const berita = new Vue({
    el: '#berita1',
    render: h => h(Berita)
}).$mount("#berita1");

const profil = new Vue({
    el: '#profil1',
    render: h => h(Profil)
}).$mount('#profil1')

import Visi from './components/IsiVisi'

const visi = new Vue({
    el: '#visi1',
    render: h => h(Visi)
}).$mount('#visi1')