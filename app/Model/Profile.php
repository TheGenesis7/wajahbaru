<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = [
        'gambar_profile',
        'gambar_profile_2',
        'title_depan',
        'title_belakang',
        'nama_depan',
        'nama_belakang',
        'fb_link',
        'twitter_link',
        'instagram_link',
        'youtube_link',
        'line_link'
    ];

    public $appends = [
        'gambar_profile_path',
        'gambar_profile_2_path'
    ];

    public function timeline(){
        return $this->hasMany('App\Model\ProfileTimeline','id_profile');
    }

    public function getGambarProfilePathAttribute(){
        return asset('storage/'.str_replace('public/','',$this->gambar_profile));
    }

    public function getGambarProfile2PathAttribute(){
        return asset('storage/'.str_replace('public/','',$this->gambar_profile_2));
    }
}
