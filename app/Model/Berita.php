<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';
    protected $fillable = [
        'gambar','judul','excerpt','konten'
    ];
    public $appends = [
        'gambar_path'
    ];
    public function getGambarPathAttribute(){
        return asset('storage/'.str_replace('public/','',$this->gambar));
    }
}
