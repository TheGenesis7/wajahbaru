<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){}

    public function profile()
    {
        $user = Auth::user();
        return view('pages.user-profile', compact('user'));
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'email|required',
        ]);
        $data = $request->input();
        if ($request->has('password')) {
            if ($data['password'] != '') {
                $data['password'] = bcrypt($data['password']);
            } else {
                unset($data['password']);
            }

        }
        $user = User::find(Auth::user()->id);
        $user->update($data);
        return redirect()->route('user-profile')->with('success','Berhasil Mengupdate Profile User');
    }

}
