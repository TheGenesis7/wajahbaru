<!--------------------
       START - Mobile Menu
       -------------------->
<div class="menu-mobile menu-activated-on-click color-scheme-dark">
    <div class="mm-logo-buttons-w">
        <a class="mm-logo" href="{{url('')}}"><img src="{{asset('assets/img/logo.png')}}"/><span>SiBisa</span></a>
        <div class="mm-buttons">
            <div class="content-panel-open">
                <div class="os-icon os-icon-grid-circles"></div>
            </div>
            <div class="mobile-menu-trigger">
                <div class="os-icon os-icon-hamburger-menu-1"></div>
            </div>
        </div>
    </div>
    <div class="menu-and-user">
        <div class="logged-user-w">
            <div class="avatar-w">
                <img alt="" src="{{asset('assets/img/img-user.png')}}"/>
            </div>
            <div class="logged-user-info-w">
                <div class="logged-user-name">
                    {{auth()->user()->nama}}
                </div>
                <div class="logged-user-role">
                    Admin
                </div>
            </div>
        </div>
        <!--------------------
        START - Mobile Menu List
        -------------------->
        <ul class="main-menu">
            <li class="">
                <a href="{{route('web')}}">
                    <div class="icon-w">
                        <div class="os-icon os-icon-monitor"></div>
                    </div>
                    <span>Web Setting</span></a>
            </li>
            <li class="">
                <a href="{{route('berita')}}">
                    <div class="icon-w">
                        <div class="os-icon os-icon-newspaper"></div>
                    </div>
                    <span>Berita</span></a>
            </li>
            <li class="">
                <a href="{{route('profile')}}">
                    <div class="icon-w">
                        <div class="os-icon os-icon-user"></div>
                    </div>
                    <span>Profile</span></a>
            </li>
            <li class="">
                <a href="{{route('relawan')}}">
                    <div class="icon-w">
                        <div class="os-icon os-icon-users"></div>
                    </div>
                    <span>Relawan</span></a>
            </li>
            <li class="">
                <a href="{{route('testimoni')}}">
                    <div class="icon-w">
                        <div class="os-icon os-icon-check-circle"></div>
                    </div>
                    <span>Testimoni</span></a>
            </li>
            <li class="hidden">
                <a href="{{route('visi_misi')}}">
                    <div class="icon-w">
                        <div class="os-icon os-icon-agenda-1"></div>
                    </div>
                    <span>Visi Misi</span></a>
            </li>
        </ul>

        <!--------------------
        END - Mobile Menu List
        -------------------->
    </div>
</div>
<!--------------------
END - Mobile Menu
--------------------><!--------------------
        START - Main Menu
        -------------------->
<div
    class="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
    <div class="logo-w">
        <a class="logo" href="index.html">
            <div class="logo-element"></div>
            <div class="logo-label">
                WajahBaru
            </div>
        </a>
    </div>
    <div class="logged-user-w avatar-inline">
        <div class="logged-user-i">
            <div class="avatar-w">
                <img alt="" src="{{asset('assets/img/img-user.png')}}"/>
            </div>
            <div class="logged-user-info-w">
                <div class="logged-user-name">
                    {{auth()->user()->nama}}
                </div>
                <div class="logged-user-role">
                    Admin
                </div>
            </div>
            <div class="logged-user-toggler-arrow">
                <div class="os-icon os-icon-chevron-down"></div>
            </div>
            <div class="logged-user-menu color-style-bright">
                <div class="logged-user-avatar-info">
                    <div class="avatar-w">
                        <img alt="" src="{{asset('assets/img/img-user.png')}}"/>
                    </div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">
                            {{auth()->user()->nama}}
                        </div>
                        <div class="logged-user-role">
                            Admin
                        </div>
                    </div>
                </div>
                <div class="bg-icon">
                    <i class="os-icon os-icon-wallet-loaded"></i>
                </div>
                <ul>
                    <li>
                        <a href="{{route('user-profile')}}"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="os-icon os-icon-signs-11"></i><span>Logout</span> </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <h1 class="menu-page-header">
        Page Header
    </h1>
    <ul class="main-menu">
        <li class="sub-header">
            <span>MAIN</span>
        </li>
        <li class="">
            <a href="{{route('web')}}">
                <div class="icon-w">
                    <div class="os-icon os-icon-monitor"></div>
                </div>
                <span>Web Setting</span></a>
        </li>
        <li class="">
            <a href="{{route('berita')}}">
                <div class="icon-w">
                    <div class="os-icon os-icon-newspaper"></div>
                </div>
                <span>Berita</span></a>
        </li>
        <li class="">
            <a href="{{route('profile')}}">
                <div class="icon-w">
                    <div class="os-icon os-icon-user"></div>
                </div>
                <span>Profile</span></a>
        </li>
        <li class="">
            <a href="{{route('relawan')}}">
                <div class="icon-w">
                    <div class="os-icon os-icon-users"></div>
                </div>
                <span>Relawan</span></a>
        </li>
        <li class="">
            <a href="{{route('testimoni')}}">
                <div class="icon-w">
                    <div class="os-icon os-icon-check-circle"></div>
                </div>
                <span>Testimoni</span></a>
        </li>
        <li class="">
            <a href="{{route('visi_misi')}}">
                <div class="icon-w">
                    <div class="os-icon os-icon-agenda-1"></div>
                </div>
                <span>Visi Misi</span></a>
        </li>
    </ul>
</div>
<!--------------------
END - Main Menu
-------------------->
