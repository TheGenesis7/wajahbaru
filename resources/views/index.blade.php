<!DOCTYPE html>
<html>
@include('partials.head')
@stack('head')
<style>
    .no-form {
        display: inline !important;
        padding: 15px !important;
    }
</style>
<body class="menu-position-side menu-side-left full-screen with-content-panel" style="height: 100%">
<div class="all-wrapper with-side-panel solid-bg-all" style="min-height: 100%">
    <div class="layout-w">
       @include('partials.main-sidebar')
        <div class="content-w">
            @include('partials.navbar')
            @yield('breadcrumbs',view('partials.breadcrumbs'))
            <div class="content-i">
                @yield('content')
                <!--------------------
              START - Color Scheme Toggler
              -------------------->
                    <div class="floated-colors-btn floated-chat-btn" id="dark-mode">
                        <div class="os-toggler-w">
                            <div class="os-toggler-i">
                                <div class="os-toggler-pill"></div>
                            </div>
                        </div>
                        <span>Dark </span><span>Colors</span>
                    </div>
                    <!--------------------
                    END - Color Scheme Toggler
                    -------------------->
            </div>
            </div>
        </div>
    </div>
    <div class="display-type"></div>
</div>
@include('partials.script')
@stack('script')
<script>
    var src = $('.img-view').attr('src');
    if (src == ''){
        $('.img-view').attr('src','{{asset('assets/img/onboarding1.jpg')}}')
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(input).parent().parent().siblings().find('.img-view').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".img-input").change(function() {
        readURL(this);
    });
</script>
<script>
    $(document).ready(function () {
        if ($('.ckeditor').length) {
            $('.ckeditor').ckeditor();
        }
        if ($('.dt').length) {
            $('.dt').DataTable();
        }
        if (localStorage.getItem('dark-mode') == 1){
            $('#dark-mode').click()
            localStorage.setItem('dark-mode',1)
        }
    })
    $('#dark-mode').on('click',function(){
        if (localStorage.getItem('dark-mode') == 1){
            localStorage.setItem('dark-mode',0)
        } else {
            localStorage.setItem('dark-mode',1)
        }
    })
</script>
</body>
</html>
