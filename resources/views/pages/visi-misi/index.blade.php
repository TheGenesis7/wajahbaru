@extends('index')
@section('breadcrumbs')
    <ul class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('visi_misi')}}">Visi Misi</a>
        </li>
    </ul>
@endsection
@section('content')
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-box">
                                <div class="element-info">
                                    <div class="element-info-with-icon">
                                        <div class="element-info-icon">
                                            <div class="os-icon os-icon-agenda-1"></div>
                                        </div>
                                        <div class="element-info-text">
                                            <h5 class="element-inner-header">
                                                Visi Misi
                                            </h5>
                                            <div class="element-inner-desc">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-header">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>Data Visi Misi</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-desc">
                                    @if (session()->has('success'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true"> ×</span></button>
                                            <strong>{{session('success')}}</strong>
                                        </div>
                                    @endif
                                    <div class="table-responsive">
                                        <table class="dt table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Jenis</th>
                                                <th>Deskripsi</th>
                                                <th>..</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>Jenis</th>
                                                <th>Deskripsi</th>
                                                <th></th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            @foreach($visi_misi AS $row)
                                                <tr>
                                                    <td>{{ucfirst($row->jenis)}}</td>
                                                    <td>{{$row->deskripsi}}</td>
                                                    <td>
                                                        <a href="{{route('visi_misi.detail',['id' => $row->id])}}" class="btn btn-primary btn-detail"><i class="fa fa-edit"></i></a>
                                                        <form action="{{route('visi_misi.delete',['id' => $row->id])}}" method="post" class="no-form">
                                                            @csrf
                                                            @method('delete')
                                                            <a href="" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-header">
                                    <div class="form-group">
                                        <h5>Tambah / Edit Visi Misi</h5>
                                    </div>
                                    <form action="{{route('visi_misi.post')}}" id="form-timeline" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="id">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="">Jenis</label>
                                                    <select name="jenis" id="" class="form-control" required>
                                                        <option value="visi" {{old('jenis') == 'visi' ? 'selected' : ''}}>Visi</option>
                                                        <option value="misi" {{old('jenis') == 'misi' ? 'selected' : ''}}>Misi</option>
                                                    </select>
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="">Deskripsi</label>
                                                    <textarea class="form-control" data-error="Deskripsi Harus di isi" name="deskripsi" required>{{old('deskripsi')}}</textarea>
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-buttons-w">
                                            <button class="btn btn-primary" type="submit"> Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('.btn-detail').on('click',function (e) {
            e.preventDefault();
            $.get(this.href)
                .then(function(res){
                    $('[name=id]').val(res.id);
                    $('[name=jenis]').val(res.jenis);
                    $('[name=deskripsi]').val(res.deskripsi);
                })
        })
        $('.btn-delete').on('click',function (e) {
            e.preventDefault();
            if (confirm('Yakin Ingin Menghapus Visi Misi ?')){
                $(this).parent().submit();
                return true;
            }
            return false;
        })
    </script>
@endpush
