<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Relawan extends Model
{
    protected $table = 'relawan';
    protected $fillable = [
        'nama',
        'nik_ktp',
        'email',
        'alamat',
        'no_hp'
    ];
}
