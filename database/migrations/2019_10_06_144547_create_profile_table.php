<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gambar_profile');
            $table->string('gambar_profile_2');
            $table->string('title_depan');
            $table->string('title_belakang');
            $table->string('nama_depan');
            $table->string('nama_belakang');
            $table->string('fb_link');
            $table->string('twitter_link');
            $table->string('instagram_link');
            $table->string('youtube_link');
            $table->string('line_link');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
