<?php

namespace App\Http\Controllers;

use App\Model\Web;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index(){
        $web = Web::first();
        return view('pages.web-setting',compact('web'));
    }

    public function store(Request $request){
        $web = Web::first();
        $this->validate($request,[
            'logo' => 'image'.($web ? '' : '|required'),
            'tagline' => 'required',
            'video' => 'required',
            'deskripsi_video' => 'required',
            'facebook' => 'required',
            'twitter' => 'required',
            'instagram' => 'required',
            'email' => 'required|email',
            'youtube' => 'required',
            'alamat' => 'required'
        ]);
        if ($request->hasFile('logo')) {
            $img = $request->file('logo')->store('public/logo');
            $data = array_merge($request->all(),[
                'logo' => $img
            ]);
        } else {
            $data = $request->all();
        }

        if ($web){
            if ($request->hasFile('logo')){
                if (file_exists(storage_path('app/'.$web->logo)))
                unlink(storage_path('app/'.$web->logo));
            }
            $web->update($data);
        } else {
            Web::create($data);
        }
        return redirect()->route('web')->with('success','Berhasil Menyimpan Data');
    }
}
