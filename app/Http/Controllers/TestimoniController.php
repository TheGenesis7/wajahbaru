<?php

namespace App\Http\Controllers;

use App\Model\Testimoni;
use Illuminate\Http\Request;

class TestimoniController extends Controller
{
    public function index(){
        $testimoni = Testimoni::orderBy('created_at')->get();
        return view('pages.testimoni.index',compact('testimoni'));
    }

    public function add(){
        return view('pages.testimoni.add');
    }

    public function detail($id){
        $testimoni = Testimoni::find($id);
        return view('pages.testimoni.detail',compact('testimoni'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'gambar' => 'required|image',
            'nama' => 'required',
            'testimoni' => 'required',
        ]);
        $data = $request->all();
        $data['gambar'] = $request->file('gambar')->store('public/testimoni');
        if (Testimoni::create($data)) {
            return redirect()->route('testimoni')->with('success', 'Berhasil Menambah Testimoni');
        }
        return redirect()->back()->with('error','Gagal Menambah Data');
    }

    public function update(Request $request,$id){
        $testimoni = Testimoni::find($id);
        $this->validate($request,[
            'gambar' => 'image',
            'nama' => 'required',
            'testimoni' => 'required',
        ]);
        $data = $request->all();
        if ($request->hasFile('gambar')){
            unlink(storage_path('app/'.$testimoni->gambar));
            $data['gambar'] = $request->file('gambar')->store('public/testimoni');
        }
        if ($testimoni->update($data)){
            return redirect()->back()->with('success','Berhasil Mengupdate Testimoni');
        }
        return redirect()->back()->with('error','Gagal Mengupdate Data');
    }

    public function delete($id){
        $testimoni = Testimoni::find($id);
        if (file_exists(storage_path('app/'.$testimoni->gambar)))
            unlink(storage_path('app/'.$testimoni->gambar));
        $testimoni->delete();
        return redirect()->route('testimoni');
    }

}
