@extends('index')
@section('breadcrumbs')
    <ul class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('testimoni')}}">Testimoni</a>
        </li>
        <li class="breadcrumb-item">
            <a href="">Update Testimoni</a>
        </li>
    </ul>
@endsection
@section('content')
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-box">
                                <div class="element-info">
                                    <div class="element-info-with-icon">
                                        <div class="element-info-icon">
                                            <div class="os-icon os-icon-check-circle"></div>
                                        </div>
                                        <div class="element-info-text">
                                            <h5 class="element-inner-header">
                                                Update Testimoni
                                            </h5>
                                            <div class="element-inner-desc">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-desc">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form id="formValidate" action="{{route('testimoni.update',['id' => $testimoni->id])}}" method="post" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                @method('put')
                                                @if (session()->has('success'))
                                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true"> ×</span></button>
                                                    <strong>{{session('success')}}</strong>
                                                </div>
                                                @endif
                                                @if (session()->has('error'))
                                                <div class="alert alert-error alert-dismissible fade show" role="alert">
                                                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true"> ×</span></button>
                                                    <strong>{{session('error')}}</strong>
                                                </div>
                                                @endif
                                                <fieldset class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Nama</label>
                                                                <input type="text" class="form-control" data-error="Nama Harus di isi" name="nama" value="{{$testimoni ? $testimoni->nama : old('nama')}}" required>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="">Testimoni</label>
                                                                <textarea class="form-control" data-error="Testimoni Harus di isi !" name="testimoni" id="" required>{{$testimoni ? $testimoni->testimoni : old('testimoni')}}</textarea>
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label for=""> Gambar</label>
                                                                <input accept="image/*" class="form-control img-input" name="gambar" value="" data-error="Gambar Testimoni Harus di isi !" placeholder="Gambar Testimoni" type="file" />
                                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <img src="{{$testimoni ? $testimoni->gambar_path : ''}}" alt="" class="img-view img-fluid ">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div class="form-buttons-w">
                                                    <button class="btn btn-primary" type="submit"> Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
