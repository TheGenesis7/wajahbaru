<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OAuhtClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $token = [
            'id' => 1,
            'user_id' => null,
            'name' => 'client',
            'secret' => 'secret',
            'redirect' => '',
            'personal_access_client' => 1,
            'password_client' => 1,
            'revoked' => 0
        ];

        DB::table('oauth_clients')->insert($token);
    }
}
