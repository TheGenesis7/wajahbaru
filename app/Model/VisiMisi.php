<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VisiMisi extends Model
{
    protected $table = 'visi_misi';
    protected $fillable = [
        'jenis','deskripsi'
    ];
}
