<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Web extends Model
{
    protected $table = 'web';
    protected $fillable = [
        'logo',
        'tagline',
        'video',
        'deskripsi_video',
        'facebook',
        'instagram',
        'email',
        'youtube',
        'alamat',
        'twitter',
    ];

    public $appends = [
        'logo_path'
    ];

    public function getLogoPathAttribute(){
        return asset('storage/'.str_replace('public/','',$this->logo));
    }
}
