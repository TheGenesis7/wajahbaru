<?php

namespace App\Http\Controllers;

use App\Model\ProfileTimeline;
use Illuminate\Http\Request;

class TimelineController extends Controller
{
    public function index($id_profile){
        $timeline = ProfileTimeline::where('id_profile',$id_profile)->orderBy('tahun','asc')->get();
        return view('pages.profile.timeline',compact('timeline','id_profile'));
    }

    public function post(Request $request,$id_profile){
        if ($request->has('id')){
            $timeline = ProfileTimeline::find($request->id);
        }
        $this->validate($request,[
            'tahun' => 'required',
            'deskripsi_timeline' => 'required',
            'gambar_timeline' => 'image'
        ]);
        $data = $request->all();
        $data['id_profile'] = $id_profile;
        if ($request->hasFile('gambar_timeline')) {
            if ($timeline){
                $gambar = storage_path('app/'.$timeline->gambar_timeline);
                if (file_exists($gambar))
                    unlink($gambar);
            }
            $data['gambar_timeline'] = $request->file('gambar_timeline')->store('public/timeline');
        }
        if ($timeline){
            $timeline->update($data);
            $message = 'Berhasil Mengupdate Timeline';
        } else {
            ProfileTimeline::create($data);
            $message = 'Berhasil Menambahkan Timeline';
        }
        return redirect()->back()->with('success',$message);
    }

    public function detail(Request $request,$id_profile,$id){
        $timeline = ProfileTimeline::where('id_profile',$id_profile)->find($id);
        return response()->json($timeline);
    }

    public function delete($id_profile,$id){
        $timeline = ProfileTimeline::where('id_profile',$id_profile)->find($id);
        $gambar = storage_path('app/'.$timeline->gambar_timeline);
        if (file_exists($gambar))
            unlink($gambar);
        $timeline->delete();
        return redirect()->back();
    }
}
