<?php

namespace App\Http\Controllers;

use App\Model\Berita;
use App\Model\Profile;
use App\Model\Relawan;
use App\Model\Testimoni;
use App\Model\VisiMisi;
use App\Model\Web;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getWebSetting(){
        if (!$data = Web::first()){
            $data = [];
        }
        return response()->json($data);
    }

    public function getBerita(){
        $berita = Berita::select('gambar','judul','excerpt','created_at','id')->take(4)->latest()->get();
        return response()->json($berita);
    }

    public function getDetailBerita($id){
        $berita = Berita::find($id);
        return response()->json($berita);
    }

    public function getTestimoni(){
        $testimoni = Testimoni::take(3)->latest()->get();
        return response()->json($testimoni);
    }

    public function getProfile(){
        $profile = Profile::latest()->first();
        return response()->json($profile);
    }

    public function getDetailProfile($id){
        $profile = Profile::with('timeline')->find($id);
        return response()->json($profile);
    }

    public function getVisiMisi(){
        $visi_misi = VisiMisi::get();
        return response()->json($visi_misi);
    }

    public function postRelawan(Request $request){
        $this->validate($request,[
            'nama' => 'required',
            'nik_ktp' => 'required',
            'email' => 'required|email',
            'no_hp' => 'required',
            'alamat' => 'required',
        ]);
        $data = $request->all();
        try {
            $relawan = Relawan::create($data);
            return response()->json($relawan);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'gagal menambahkan relawan',
                'error' => $exception->getMessage()
        ]);
        }
    }
}
