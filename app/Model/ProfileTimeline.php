<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProfileTimeline extends Model
{
    protected $table = 'profile_timeline';
    protected $fillable = [
        'id_profile',
        'tahun',
        'gambar_timeline',
        'deskripsi_timeline'
    ];

    public $appends = [
        'gambar_timeline_path'
    ];

    public function getGambarTimelinePathAttribute(){
        return asset('storage/'.str_replace('public/','',$this->gambar_timeline));
    }
}
